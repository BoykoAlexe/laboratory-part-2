CREATE DATABASE lab;
\c lab;

CREATE SCHEMA lab_schema;

create table lab_schema.student(id_student int,last_name text,first_name text,birth date,id_book int);
create table lab_schema.credit_book(id_book int,l_name text,e_date date,mark int);

INSERT INTO lab_schema.student VALUES (1, 'Иванов','Иван','03.04.2002',1);
INSERT INTO lab_schema.student VALUES (2, 'Пупкин','Иван','03.05.2002',2);
INSERT INTO lab_schema.student VALUES (3, 'Сидоров','Алексей','03.04.2002',3);
INSERT INTO lab_schema.student VALUES (4, 'Зернушко','Петр','05.09.2002',4);
INSERT INTO lab_schema.student VALUES (5, 'Иванов','Петр','06.08.2002',5);

INSERT INTO lab_schema.credit_book VALUES (1, 'Математика','06.08.2002',5);
INSERT INTO lab_schema.credit_book VALUES (1, 'География','06.08.2002',5);
INSERT INTO lab_schema.credit_book VALUES (1, 'Физика','06.08.2002',5);

INSERT INTO lab_schema.credit_book VALUES (2, 'Математика','06.08.2002',5);
INSERT INTO lab_schema.credit_book VALUES (2, 'География','06.08.2002',5);
INSERT INTO lab_schema.credit_book VALUES (2, 'Физика','06.08.2002',5);

INSERT INTO lab_schema.credit_book VALUES (3, 'Математика','06.08.2002',4);
INSERT INTO lab_schema.credit_book VALUES (3, 'География','06.08.2002',3);
INSERT INTO lab_schema.credit_book VALUES (3, 'Физика','06.08.2002',4);

INSERT INTO lab_schema.credit_book VALUES (4, 'Математика','06.08.2002',5);
INSERT INTO lab_schema.credit_book VALUES (4, 'География','06.08.2002',5);
INSERT INTO lab_schema.credit_book VALUES (4, 'Физика','06.08.2002',4);

INSERT INTO lab_schema.credit_book VALUES (5, 'Математика','06.08.2002',5);
INSERT INTO lab_schema.credit_book VALUES (5, 'География','06.08.2002',5);
INSERT INTO lab_schema.credit_book VALUES (5, 'Физика','06.08.2002',5);



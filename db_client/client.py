import psycopg2
import os
import time
import subprocess

time.sleep(30)

conn = psycopg2.connect(dbname='lab', user='postgres',
                        password='postgres', host='db', port=5432)
cursor = conn.cursor()



cursor.execute('select last_name, first_name from lab_schema.student where id_book in (select id_book from lab_schema.credit_book group by id_book having min(mark) = 5);')
records = cursor.fetchall()

with open(os.path.join("output.txt"), "w+") as file:
    for i in range(len(records)):
        print(f'{records[i][0]} {records[i][1]}')
        file.write(f'{records[i][0]} {records[i][1]}\n')


print(os.system('ls'))
print(os.system('pwd'))



cursor.close()
conn.close()
